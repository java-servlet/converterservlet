#!/bin/bash 

#version 0.1 alpha 

default_filename=deploy.war 
serverlocation=/usr/local/tomcat-9/webapps/
warlocation=build/libs/

if cd $warlocation; then {
    if ls *.war; then { 
        mv *.war $default_filename
        bash /usr/local/tomcat-9/bin/catalina.sh stop 
        mv -v $default_filename $serverlocation
        bash /usr/local/tomcat-9/bin/catalina.sh start
    } fi
} fi

