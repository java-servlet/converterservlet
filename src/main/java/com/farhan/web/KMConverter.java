package com.farhan.web;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/km")
public class KMConverter extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/km_to_mile_user_input.jsp").forward(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        var km = req.getParameter("km");

        if (km != null && km.length() > 0) {
            //double temperatureInC = Double.parseDouble(temperature);
            //double temperatureInF = (temperatureInC * 9/5) + 32;

            // 1 km = 0.62137119 miles.
            double kilometer = Double.parseDouble(km);
            double calculate = kilometer * 0.62137119;

            req.setAttribute("result", calculate);
        }

        req.getRequestDispatcher("/WEB-INF/km_to_mile_output.jsp").forward(req, resp);

    }
}
