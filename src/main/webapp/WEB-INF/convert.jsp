<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Temperature Converter</title>
    </head>
    <body>

        <h1>Celsius to Fahrenheit conversion</h1>
        <p>Version 0.3</p>
        <form method="post" action="${pageContext.request.contextPath}/temp">
            <input type="number" name="temperature"/>
            <input type="submit" value="Submit"/>
        </form>
    </body>
</html>