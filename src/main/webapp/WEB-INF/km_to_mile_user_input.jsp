<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Temperature Converter</title>
    </head>
    <body>

        <h1>Kilometer to Mile</h1>
        <p>Version 0.4 Alpha</p>
        <form method="post" action="${pageContext.request.contextPath}/km">
            <input type="number" name="km"/>
            <input type="submit" value="Submit"/>
        </form>
    </body>
</html>